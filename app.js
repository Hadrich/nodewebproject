var http = require('http');
var fs = require('fs');
var utils = require('utils');
var	mongoose = require('mongoose');
var express = require('express');
var app = express();
var path = require("path");
var request = require('request');
var cheerio = require('cheerio');
var uriUtil = require('mongodb-uri');
var phantom = require('phantom');
var config = require('./config.json');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';	

function decrypt(text){
  var decipher = crypto.createDecipher(algorithm,password)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}
 
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

var bodyParser = require('body-parser'); 
app.use(bodyParser.json()); // to support JSON bodies
app.use(bodyParser.urlencoded({ extended: true })); // to support URL-encoded bodies
function stock(symbol,earning,predicted,last,eps)
{
	this.symbol=symbol ;
	this.earning=earning ; 
	this.predicted=predicted ;
	this.last = last ;
	this.eps =eps ;
}
var i=0; 
var liststock = [];


var mongo = require('mongodb'),
 Server = mongo.Server,
 Db = mongo.Db;
 
var casper = require('casper').create();
var x = require('casper').selectXpath ;
// Try

var uri =config.remote;

var db = mongoose.connect(uri);
//db = mongoose.createConnection(mongooseUri);
var Schema = mongoose.Schema;

  var stockSchema = new Schema({
      symbol  :  { type: String, default: '' }
    , earning   :  { type: String, default: '' }
	, predicted  :  {type: Number }
	, lastEarn   :  { type: Number }
	, eps   :  { type: Number }
  });
  var stockModel = mongoose.model('stock', stockSchema);


// Set up the spooky agent to retrieve URLs
var webdriverio = require('webdriverio');
var options = { desiredCapabilities: { browserName: 'firefox' } };
var client = webdriverio.remote(options);
var body1 ;
client
    .init()
    .url(config.url)
	.click('#lnkbtnLogin1')
    .setValue('#txtUserName', decrypt(config.user))
	.setValue('#txtPassword',decrypt(config.password))
    .click('#imgbtnLogin')
	.getSource().then(function(source) {
		 body1 = source ; 
	})
	.url(config.url,function (req,res){
		var target = config.url;
		request(target,function(err,response,body){
			//console.log(body1);
			$ = cheerio.load(body1);
		
//data extraction manipulation	
$("tr.mybg").each(function(index,table){
			
			var symbol = $(table).find('.csy');
			var date = $(table).find('.csy2');
			var move = $(table).find('td').eq(2);
			var lastEarning = $(table).find('td').eq(3);
			var eps = $(table).find('td').eq(4);
			symbol = $(symbol).text().replace(/\s/g, '');
			//prepared symbol
			symbol = symbol.substring(0,symbol.indexOf('-'));
			
			
			console.log(symbol);
			
			date = $(date).text().replace(/\s/g, '');
			//prepared date
	
			console.log(date);
			
		
			move = $(move).text().replace(/\s/g, '');
			move = move.substring(0,move.indexOf('%'));
			lastmove=parseInt(move);
			
			//prepared lastmove
			console.log(lastmove);
			
			lastEarning = $(lastEarning).text().replace(/\s/g, '');
			lastEarning = lastEarning.substring(0,lastEarning.indexOf('%'));
			lastEarn = parseFloat(lastEarning);
		
			//prepared lastEarn
			console.log(lastEarn);
		
			eps1 = $(eps).text().replace(/\s/g, '');
			if (eps = "")
			{
				eps="0";
			}
			eps = parseFloat(eps1);
			console.log(eps);
		
			liststock[i] = new stock(symbol,date,lastmove,lastEarn,eps);
			var test = new stockModel({symbol:liststock[i].symbol, earning:liststock[i].earning, predicted:liststock[i].predicted, lastEarn:liststock[i].last,eps:liststock[i].eps});
 //Insert. 

 	        test.save(function (err, test) {
            console.log("saved?")
            if (err) {
           console.log("error");
           return console.error(err);
            }
    console.log("saved!")
  });

			
			i++;
				
	 });
	 
			
			
		});

	
		
	});	//END
	



// 

 app.get('/', function(req, res)
 { 
 		res.writeHead(200,{"Content-Type":"text/html"});
		fs.createReadStream("./index.html").pipe(res);
		//res.render('index', {liststock:liststock});
 });
 
 
 
 
 
 
 app.post('/New', function(req, res){	 
//try
//end try	
 
//extarction process 
if (req.body.optradio == "0" )
	 {
var server = new Server(config.local, 27017, {auto_reconnect: true});
var database = new Db('market', server);
database.open(function(err, db) 
{ if(!err) 
{ console.log("connected");
db.collection('stocks',function(err,coll){ 
for (i=0;i<liststock.length;i++)
{
var stockMarket ={symbol:liststock[i].symbol, earning:liststock[i].earning, predicted:liststock[i].predicted, lastEarn:liststock[i].last,eps:liststock[i].eps};
 //Insert. 
coll.insert(stockMarket,function (err)
 { if(err) 
	 console.log("error"); 
 else 
	 console.log('inserted data was success');

 }); 
}
//db.close();
});
 }
 
 }); 
 }

 //end db local
	res.render('index1', {list:liststock});
	//liststock = [];
});	 
	 
//end remote	 

app.listen(8888); 
console.log('Server is running on port 8888');